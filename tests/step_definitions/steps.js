const { I } = inject();

Given('я нахожусь на странице регистрации', () => {
 I.amOnPage('/register');
});

Given('я нахожусь на странице логина', () => {
  I.amOnPage('/login');
});

When('я ввожу в поля текст:', table => {
  for (const id in table.rows) {
    if (id < 1) {
      continue; // skip a header of a table
    }

    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When('нажимаю на кнопку {string}', (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);
});

When('вижу {string}', str => {
  I.see(str);
});
