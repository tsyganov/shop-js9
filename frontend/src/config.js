const env = process.env.REACT_APP_ENV;

let domain = 'localhost:8000';

if (env === 'test') {
  domain = 'localhost:8010';
} else if (env === 'production') {
  domain = '64.227.79.150:8000';
}

export const apiDomain = domain;
export const apiURL = 'http://' + apiDomain;
export const wsURL = 'ws://' + apiDomain;
export const googleClientId = process.env.REACT_APP_GOOGLE_CLIENT_ID;
export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;