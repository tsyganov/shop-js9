import React, {useEffect, useRef, useState} from 'react';
import {wsURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {addNotification} from "../../store/actions/notifierActions";

const Chat = () => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.users.user.token);
  const ws = useRef(null);

  const [users, setUsers] = useState([]);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    ws.current = new WebSocket(wsURL + '/chat?token=' + token);

    ws.current.onmessage = msg => {
      try {
        const parsed = JSON.parse(msg.data);

        switch (parsed.type) {
          case 'LOGGED_IN':
            setUsers(parsed.users);
            setMessages(parsed.messages.reverse());
            break;
          case 'NEW_MESSAGE':
            setMessages(prev => [...prev, parsed.message]);
            break;
          case 'USER_LOGGED_IN':
            setUsers(prev => [...prev, parsed.user]);
            break;
          case 'USER_LOGGED_OUT':
            setUsers(prev => prev.filter(u => u._id !== parsed.userId));
            break;
          case 'ERROR':
            dispatch(addNotification({message: parsed.message, options: {variant: 'error'}}));
            break;
          default:
            dispatch(addNotification({message: 'Unknown message', options: {variant: 'error'}}));
        }
      } catch (e) {
        console.log(e);
        dispatch(addNotification({message: 'Wrong message format', options: {variant: 'error'}}))
      }
    };

    return () => {
      ws.current.close();
    };
  }, [dispatch, token]);

  return (
    <div>
      <div>Logged in users:</div>
      <div>
        {users.map(u => (
          <div key={u._id}>
            {u.name}
          </div>
        ))}
      </div>
      <div>
        {messages.map(m => (
          <div key={m._id}>{m.text}</div>
        ))}
      </div>
    </div>
  );
};

export default Chat;