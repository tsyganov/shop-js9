require('dotenv').config();
const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose');
const expressWs = require('express-ws');
const exitHook = require('async-exit-hook');

const config = require('./config');
const products = require('./app/products');
const categories = require('./app/categories');
const users = require('./app/users');
const chat = require('./app/chat');
const auth = require("./middleware/auth");

const app = express();
app.use(cors());
expressWs(app);
app.use(express.static('public'));
app.use(express.json());

app.use('/products', products);
app.use('/categories', categories);
app.use('/users', users);
app.ws('/chat', chat);

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(config.port, () => {
    console.log(`Server started on ${config.port} port!`);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    console.log('mongoose disconnected');
    callback();
  });
}

run().catch(console.error);