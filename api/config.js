const path = require('path');
const rootPath = __dirname;

const env = process.env.NODE_ENV;
const dbhost = process.env.DB_HOST || 'localhost';

let databaseUrl = `mongodb://${dbhost}/shop`;
let port = 8000;

if (env === 'test') {
  databaseUrl = 'mongodb://localhost/shop_test';
  port = 8010;
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public'),
  port,
  db: {
    url: databaseUrl,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: process.env.FACEBOOK_APP_ID,
    appSecret: process.env.FACEBOOK_APP_SECRET,
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID
  }
};