const User = require("../models/User");
const ChatMessage = require("../models/ChatMessage");

const activeConnections = {};

const chat = async (ws, req) => {
  const token = req.query.token;

  if (!token) {
    return ws.close(401);
  }

  const user = await User.findOne({token});

  if (!user) {
    return ws.close(401);
  }

  const loggedInUsers = Object.keys(activeConnections).map(c => ({
    _id: activeConnections[c].user._id,
    name: activeConnections[c].user.displayName
  }));

  const lastMessages = await ChatMessage.find()
    .populate('user', 'displayName')
    .sort({'date': -1})
    .limit(30);

  ws.send(JSON.stringify({
    type: 'LOGGED_IN',
    users: loggedInUsers,
    messages: lastMessages,
  }));

  activeConnections[user._id] = {ws, user};

  Object.keys(activeConnections).forEach(c => {
    activeConnections[c].ws.send(JSON.stringify({type: 'USER_LOGGED_IN', user: {_id: user._id, name: user.displayName}}));
  });

  ws.on('message', async msg => {
    try {
      const parsed = JSON.parse(msg);

      switch (parsed.type) {
        case 'CREATE_MESSAGE':
          const message = await ChatMessage.create({
            user,
            text: parsed.text,
            date: (new Date()).toISOString()
          });

          Object.keys(activeConnections).forEach(c => {
            activeConnections[c].ws.send(JSON.stringify({type: 'NEW_MESSAGE', message}))
          });
          break;
        default:
          ws.send(JSON.stringify({type: 'ERROR', message: 'Wrong message type'}));
      }
    } catch (e) {
      ws.send(JSON.stringify({type: 'ERROR', message: 'Incorrect message format, please send JSON'}));
    }
  })

  ws.on('close', () => {
    Object.keys(activeConnections).forEach(c => {
      const conn = activeConnections[c];

      if (conn.ws !== ws) {
        conn.ws.send(JSON.stringify({type: 'USER_LOGGED_OUT', userId: user._id}));
      }
    });

    delete activeConnections[user._id];
  });
};

module.exports = chat;